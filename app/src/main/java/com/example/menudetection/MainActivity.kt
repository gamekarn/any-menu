package com.example.menudetection


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.adityaarora.liveedgedetection.activity.ScanActivity
import com.adityaarora.liveedgedetection.constants.ScanConstants
import com.adityaarora.liveedgedetection.util.ScanUtils
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.bitmap
import com.google.firebase.ml.vision.common.FirebaseVisionImage




class MainActivity : AppCompatActivity() {
    val REQUEST_CODE = 1;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCamera.setOnClickListener {
            startActivityForResult(Intent(this, ScanActivity::class.java), REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val filePath = data!!.getExtras()!!.getString(ScanConstants.SCANNED_RESULT)
        val baseBitmap = ScanUtils.decodeBitmapFromFile(filePath, ScanConstants.IMAGE_NAME)
    }
}



